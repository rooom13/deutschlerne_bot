## @dailyWordBot
**@dailyWordBot** is a Telegram Bot to help Spanish speakers learn German &amp; vice-versa developed using Node.js and Redis.

This bot will send to you random words with sentence examples in both german and spanish in order to learn new vocabulary.
Also includes a translation feature.

Hosted in a VM using Docker as a container platform.
